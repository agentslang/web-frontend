/**
 * Author: Ovidiu Serban, ovidiu@roboslang.org
 * Date: 26/04/16
 */

audiojs.events.ready(function () {
    var audioTag = document.getElementsByTagName('audio')[0];

    window.audio = audiojs.create(audioTag, {
        css: false,
        createPlayer: {
            markup: false,
            playPauseClass: 'play-pauseZ',
            scrubberClass: 'scrubberZ',
            progressClass: 'progressZ',
            loaderClass: 'loadedZ',
            timeClass: 'timeZ',
            durationClass: 'durationZ',
            playedClass: 'playedZ',
            errorMessageClass: 'error-messageZ',
            playingClass: 'playingZ',
            loadingClass: 'loadingZ',
            errorClass: 'errorZ'
        }
    });

    audioTag.addEventListener('play', function () {
        wsSendStatusMessage('player-play');
    }, false);

    audioTag.addEventListener('pause', function () {
        wsSendStatusMessage('player-pause');
    }, false);

    audioTag.addEventListener('ended', function () {
        wsSendStatusMessage('player-ended');
    }, false);

    systemReady();
});

