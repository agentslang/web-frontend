/**
 * Author: Ovidiu Serban, ovidiu@roboslang.org
 * Date: 26/04/16
 */

$(document).ready(function () {
    wsInit();
});

var wsClient;

function wsInit() {
    if ("WebSocket" in window) {
        console.info("WebSocket is supported by your Browser!");
        wsClient = new WebSocket(wsUrl);

        wsClient.onopen = function () {
            console.info("Connection open with url: " + wsUrl);
            $("#btn-disconnected").hide();
            $("#btn-connected").show();

            systemReady();
        };

        wsClient.onmessage = function (evt) {
            // create a data protocol for debug messages
            var receivedMsg = evt.data;
            console.info("Message is received: " + receivedMsg);
            wsReceiveMsg(receivedMsg)
        };

        wsClient.onclose = function () {
            console.info("Connection is closed...");
            $("#btn-disconnected").show();
            $("#btn-connected").hide();
        };
    } else {
        // The browser doesn't support WebSocket
        console.error("WebSocket NOT supported by your Browser!");
    }
}

function wsSendMsg(message) {
    if (wsReady()) {
        if (message != null && message.length > 0) {
            var strMsg = JSON.stringify({
                type: "message",
                message: message,
                voiceUrl: ""
            });

            wsClient.send(strMsg);
            console.info("Message sent: " + strMsg);
        }
    }
}

function wsSendStatusMessage(status) {
    if (wsReady()) {
        if (status != null && status.length > 0) {
            var strMsg = JSON.stringify({
                type: "system",
                message: status,
                voiceUrl: ""
            });

            wsClient.send(strMsg);
            console.info("Message sent: " + strMsg);
        }
    }
}

function wsReceiveMsg(strMsg) {
    var message = JSON.parse(strMsg);

    switch (message.type) {
        case "message":
            agentSpeak(message.message, message.voiceUrl);
            break;
        case "debug":
            agentDebug(message.message);
            break;
    }
}


function wsReady() {
    return wsClient.readyState == 1;
}

function systemReady() {
    if (wsReady() && window.audio != null) {
        wsSendStatusMessage("ready");
    }
}

