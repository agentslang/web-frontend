/**
 * Author: Ovidiu Serban, ovidiu@roboslang.org
 * Date: 26/04/16
 */

$("#search-input").on("keydown", function (e) {
    if (e.keyCode === 13) {
        userSpeak();
    }
});

function userSpeak() {
    var searchResult = document.getElementById("search-input").value;
    if (wsReady() && searchResult != null && searchResult.length > 0) {
        var searchResultItem =
            $("<div />",
                {"class": "list-group-item alert alert-info"}
            ).append(
                $("<h4 />",
                    {
                        "class": "list-group-item-heading",
                        text: " User Query "
                    }).prepend('<span class="glyphicon glyphicon-user" aria-hidden="true"></span>')
                    .append('<span class="label label-default">' + new Date().toLocaleString() + '</span>')
            ).append(
                $("<p />",
                    {
                        "class": "list-group-item-text",
                        text: searchResult
                    })
            );
        $("#search-result").prepend(searchResultItem);
        $("#search-input").val('');

        wsSendMsg(searchResult);
    }
}

function agentSpeak(replyText, playUrl) {
    if (replyText != null && replyText.length > 0) {
        var searchResultItem =
            $("<div />",
                {"class": "list-group-item alert alert-success"}
            ).append(
                $("<h4 />",
                    {
                        "class": "list-group-item-heading",
                        text: " AgentSlang "
                    }).prepend('<span class="glyphicon glyphicon-comment" aria-hidden="true"></span>')
                    .append('<span class="label label-default">' + new Date().toLocaleString() + '</span>')
            ).append(
                $("<p />",
                    {
                        style: "margin: 10px 0 0 0;",
                        "class": "list-group-item-text",
                        text: replyText
                    })
            );
        $("#search-result").prepend(searchResultItem);

        if (playUrl != null && playUrl.length > 0) {
            window.audio.load(playUrl);
            window.audio.play();
        }
    }
}

function agentDebug(debugText) {
    if (debugText != null && debugText.length > 0) {
        var searchResultItem =
            $("<div />",
                {"class": "list-group-item alert alert-danger"}
            ).append(
                $("<h4 />",
                    {
                        "class": "list-group-item-heading",
                        text: " [Debug] AgentSlang "
                    }).prepend('<span class="glyphicon glyphicon-fire" aria-hidden="true"></span>')
                    .append('<span class="label label-default">' + new Date().toLocaleString() + '</span>')
            ).append(
                $("<p />",
                    {
                        "class": "list-group-item-text",
                        text: debugText
                    })
            );
        $("#search-result").prepend(searchResultItem);
    }
}